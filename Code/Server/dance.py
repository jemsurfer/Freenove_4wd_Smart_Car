from Led import *
from Motor import *
from servo import * 
import asyncio
import random
from time import sleep

led = Led()
motor = Motor()
pwm = Servo()
async def led_disco():
    try:
        while True:
            print ("Chaser animation")
            led.colorWipe(led.strip, Color(255,0, 0))  # Red wipe
            led.colorWipe(led.strip, Color(0, 255, 0))  # Green wipe
            led.colorWipe(led.strip, Color(0, 0, 255))  # Blue wipe
            led.theaterChaseRainbow(led.strip)
            print ("Rainbow animation")
            led.rainbow(led.strip)
            led.rainbowCycle(led.strip)
    except:
        led.colorWipe(led.strip, Color(0,0,0),10)
        print("Stopping LEDs")

async def spin_head():
    try:
        while True:
            for i in range(50,110,1):
                pwm.setServoPwm('0',i)
                time.sleep(0.01)
            for i in range(110,50,-1):
                pwm.setServoPwm('0',i)
                time.sleep(0.01)
            for i in range(80,150,1):
                pwm.setServoPwm('1',i)
                time.sleep(0.01)
            for i in range(150,80,-1):
                pwm.setServoPwm('1',i)
                time.sleep(0.01)   
    except:
        pwm.setServoPwm('0',90)
        pwm.setServoPwm('1',90)
        print("Stopping servos")

async def rotate():
    try:
        while True:
            match random.randint(0,3):
                case 0:
                    #Forwards
                    motor.setMotorModel(2000, 2000, 2000, 2000)  
                    sleep(1)
                case 1:
                    #Backwards
                    motor.setMotorModel(-1000,-1000,-1000,-1000)
                    sleep(1)
                case 2:
                    #Left
                    motor.setMotorModel(-1500,-1500,2000,2000)
                    sleep(1)
                case 3:
                    #Right
                    motor.setMotorModel(2000,2000,-1500,-1500)
                    sleep(1)
                case _:
                    print("Random number generation is broken")
                    exit(1)
    except:
        print("Stopping motor")
        motor.setMotorModel(0, 0, 0, 0)

async def main():
    await asyncio.gather(led_disco(), rotate(), spin_head())
